#X_DOMAINS = '*'

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

XML = False

posts_schema = {
    'score': {
        'type': 'integer',
        'min': 0,
        'default': 0,
    },
    'vote': {
        'type': 'number',
        'min': 0,
        'max': 10,
        'default': 0,
    },
    'url': {
        'required': True,
        'unique': True,
    },
    'tags': {
        'type': 'list',
        'schema': {
            'type': 'string',
            'minlength': 3,
            'maxlength': 25,
        },
        'default': [],
    },
    'rating': {
        'type': 'string',
        'allowed': ['sfw', 'nsfw'],
        'default': 'sfw',
    }
}

posts = {
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,
    
    'resource_methods': ['GET', 'POST'],
    
    'schema': posts_schema,
}

DOMAIN = {
    'posts': posts,
}